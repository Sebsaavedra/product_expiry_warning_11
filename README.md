Expiry Date Warning
===================

Generates warning for expired products, when we sell the product.

Installation
============
- www.odoo.com/documentation/11.0/setup/install.html
- Install our custom addon

License
=======
GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (LGPLv3)
(http://www.gnu.org/licenses)

Bug Tracker
===========
Bugs are tracked on GitHub Issues. In case of trouble, please check there if your issue has already been reported.

Credits
=======
* Cybrosys Techno Solutions <https://www.cybrosys.com>

Author
------

Developers: Odoo-10.0, Avinash Nk(odoo@cybrosys.com)
	    Odoo-11.0, Meera K (odoo@cybrosys.com)

Maintainer
----------

This module is maintained by Cybrosys Technologies.

For support and more information, please visit https://www.cybrosys.com.
